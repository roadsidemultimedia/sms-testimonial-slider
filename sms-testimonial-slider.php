<?php
/*
Plugin Name: Testimonial Slider
Plugin URI: http://www.roadsidemultimedia.com
Description: Testimonial Slider with side navigation. 
Author: Curtis Grant
PageLines: true
Version: 1.1.0
Section: true
Class Name: TestimonialSliderV2
Filter: slider, gallery, roadside
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-testimonial-slider
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;

global $TS_slider_num;
$TS_slider_num = 0;

class TestimonialSliderV2 extends PageLinesSection {

  function section_styles(){
    wp_enqueue_script( 'lightSlider-js', $this->base_url.'/js/jquery.lightSlider.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_style( 'lightSlider-css', $this->base_url.'/css/lightSlider.css');
    wp_enqueue_style( 'TestimonialSlider-css', $this->base_url.'/css/TestimonialSlider.css');
  }

  function section_opts(){


    $options = array();    
    
    $options[] =
      array(
        'type'  => 'multi',
        'col'   => 2,
        'title' => 'Slider Options',
        'opts'  => array(
          array(
            'type'      => 'color',
            'key'     => 'qm_color',
            'label'     => 'Quotation Mark Color',
            'col' => 2,
          ),
          array(
            'type'      => 'select',
            'key'     => 'TS_nav_font',
            'label'     => 'Font Style for Nav',
            'col' => 2,
            'opts'      => array(
              'font-serif'   => array('name' => 'Serif'),
              'font-serif-alt'  => array('name' => 'Serif (alt)'),
              'font-sans-serif'   => array('name' => 'Sans Serif'),            
              'font-condensed-serif'    => array('name' => 'Condensed Serif'),
              'font-condensed-sans-serif' => array('name' => 'Condensed Sans Serif'),
              'font-slab-serif'  => array('name' => 'Slab Serif'),
              'font-script'  => array('name' => 'Script'),
              'font-descriptive'  => array('name' => 'Decorative'),
            )
          ),
          array(
            'type'      => 'select',
            'key'     => 'TS_nav_font_size',
            'label'     => 'Font Size for Nav',
            'col' => 2,
            'opts'      => array(
              'fs-mini'   => array('name' => 'Mini'),
              'fs-small'  => array('name' => 'Small'),
              'fs-medium'   => array('name' => 'Medium'),            
              'fs-large'    => array('name' => 'Large'),
              'fs-xlarge' => array('name' => 'X-Large'),
              'fs-xxlarge'  => array('name' => 'XX-Large'),
              'fs-xxxlarge'  => array('name' => 'XXX-Large'),
              'fs-xxxxlarge'  => array('name' => 'XXXX-Large'),
            )
          ),
          array(
            'type'      => 'select',
            'key'     => 'TS_slide_font',
            'label'     => 'Font Style for Slider',
            'col' => 2,
            'opts'      => array(
              'font-serif'   => array('name' => 'Serif'),
              'font-serif-alt'  => array('name' => 'Serif (alt)'),
              'font-sans-serif'   => array('name' => 'Sans Serif'),            
              'font-condensed-serif'    => array('name' => 'Condensed Serif'),
              'font-condensed-sans-serif' => array('name' => 'Condensed Sans Serif'),
              'font-slab-serif'  => array('name' => 'Slab Serif'),
              'font-script'  => array('name' => 'Script'),
              'font-descriptive'  => array('name' => 'Decorative'),
            )
          ),
          )
);
$options[] =
      array(
        'key'   => 'array',
        'type'    => 'accordion', 
        'col'   => 1,
        'title' => 'Slides',
        'opts_cnt'  => 5,
        'title'   => __('Text Slider Slides', 'pagelines'), 
        'opts'  => array(
          array(
              'key'   => 'TS_hdr',
              'label'   => __( 'Slider Title', 'pagelines' ),
              'type'    => 'text',
                ),
          array(
              'key'   => 'TS_text',
              'label'   => __( 'Testimonial', 'pagelines' ),
              'type'    => 'textarea',
                ),
          )
      );

    
    return $options;

  }


  function get_content( $array ){
    
    
    $out = '';
    $num = 1;
    $nav[] = array();
    $slides[] = array();
    if( is_array( $array ) ){
      foreach( $array as $key => $item ){
        $tshdr = pl_array_get( 'TS_hdr', $item );
        $tstext = pl_array_get( 'TS_text', $item ); 
        if($num == 1) { $tclass='active';} else { $tclass='inactive';}
        if( $tshdr ){
         
          $out .= sprintf(
            '<li class="%s" id="ts1slide%s"><span>%s</span></li>',
            $tclass,
            $num,
            $tshdr
          );
        }
      $num++;
      }
    }
    
    
    return $out;
  }
  function make_links( $array ){
    
    
    $out = '';
    $num = 1;
    $numzero = 0;
    if( is_array( $array ) ){
      foreach( $array as $key => $item ){
        $tshdr = pl_array_get( 'TS_hdr', $item );
        if( $tshdr ){
         
          $out .= sprintf(
            "$('#ts1slide%s').click(function(){ slider.goToSlide(%s); });",
            $num,
            $numzero
          );
        }
      $num++;
      $numzero++;
      }
    }
    
    
    return $out;
  }
  function get_slides( $array ){
    
    
    $out = '';
    $num = 1;
    $nav[] = array();
    $slides[] = array();
    if( is_array( $array ) ){
      foreach( $array as $key => $item ){
        $tshdr = pl_array_get( 'TS_hdr', $item );
        $tstext = pl_array_get( 'TS_text', $item );
        $qmsize= ( $this->opt('qm_size') ) ? $this->opt('qm_size') : "fs-xxxlarge";
        $qmcolor= ( $this->opt('qm_color') ) ? $this->opt('qm_color') : "ff5d00";
        $qm = "<span class='drop'><span class='smsquote smsquote-left' style='color:#$qmcolor'>&#8220;</span></span>"; 
        $ptn = "/^/";  // Regex
        $str = $tstext; //Your input, perhaps $_POST['textbox'] or whatever
        $rpltxt = "$qm";  // Replacement string
        $tstext = preg_replace($ptn, $qm, $str); 
        $tstext = wpautop($tstext);
        if( $tshdr ){
         
          $out .= sprintf(
            "<li>%s</li>",

            $tstext
          );
        }
      $num++;
      }
    }
    
    
    return $out;
  }

   function section_template( ) { 
    
    $array = $this->opt('array');
    $tsnavfont = ( $this->opt('TS_nav_font') ) ? $this->opt('TS_nav_font') : "";
    $tsnavfontsize = ( $this->opt('TS_nav_font_size') ) ? $this->opt('TS_nav_font_size') : "";
    $tsslidefont = ( $this->opt('TS_slide_font') ) ? $this->opt('TS_slide_font') : "";

  global $rsbasicv2_slider_num;
   if(!$rsbasicv2_slider_num) {
     $rsbasicv2_slider_num = 1;
   }
   ?>
  <div class="tssidenav <?php echo $tsnavfont; ?> <?php echo $tsnavfontsize; ?> ">
    <ul id="main-slider-nav1">
    <?php

    $out = $this->get_content( $array );

    echo $out;

    ?>
    </ul>
  </div>
  <div class="tsslider sms-quote <?php echo $tsslidefont; ?> ">
    <ul id="lightSlider">
    <?php

    $slides = $this->get_slides( $array );

    echo $slides;

    ?>
    </ul>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function($) {
      var slider = $("#lightSlider").lightSlider({
        slideWidth:980,
        slideMargin:80,
        slideMove:1,
        minSlide:1,
        maxSlide:1,
        pager:false,
        controls:false,
        keyPress:false,
        gallery:false,
        auto: false,
        pause: 2000,
        loop:false,
        easing: '',
        speed: 1000,
        mode:"slide",
        swipeThreshold:1200,
    });
  <?php

    $navlinks = $this->make_links( $array );

    echo $navlinks;

    ?>
  $(function(){
            var sidebar = $('#main-slider-nav1');
            sidebar.delegate('li.inactive','click',function(){
              sidebar.find('.active').toggleClass('active inactive');
              $(this).toggleClass('active inactive');
              var i = $(this).index();
              api.setNextPanel(i);
            });
        });
    });
  </script>

<?php 
$rsbasicv2_slider_num++;
}


}